#include <iostream>
using namespace std;
#include "Vector.h"

int main()
{
	Vector v(5);
	cout << "----------" << endl;
	cout << "capcity: " << v.capacity() << endl;
	cout << "----------" << endl;
	v.push_back(2);
	v.push_back(35);
	for (int i = 0; i < v.size(); i++)
	{
		cout << v.getArr()[i] << endl;
	}
	cout << "----------" << endl;
	cout << "popped " << v.pop_back() << endl;
	cout << "----------" << endl;
	for (int i = 0; i < v.size(); i++)
	{
		cout << v.getArr()[i] << endl;
	}
	cout << "----------" << endl;
	v.push_back(4);
	v.push_back(5);
	v.push_back(54);
	v.push_back(5334);
	v.push_back(2);
	for (int i = 0; i < v.size(); i++)
	{
		cout << v.getArr()[i] << endl;
	}
	cout << "----------" << endl;
	cout << "capcity: " << v.capacity() << endl;
	cout << "----------" << endl;
	cout << "4th int: " << v[3] << endl;
	cout << "----------" << endl;
	return 0;
}