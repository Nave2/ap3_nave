#include "Vector.h"
#include <iostream>
using namespace std;

Vector::Vector(int n)
{
	this->_size = 0;
	this->_capacity = n;
	if (this->_capacity < 2) this->_capacity = 2;
	this->_resizeFactor = this->_capacity;
	this->_elements = new int[this->_capacity];
}

Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return this->_size == 0;
}

void Vector::push_back(const int& val)
{
	if (this->_size < this->_capacity) {
		this->_elements[this->_size] = val;
		this->_size++;
	}
	else
	{
		int* newArr = new int[this->_capacity + this->_resizeFactor];
		for (int i = 0; i < this->_size; i++)
			newArr[i] = this->_elements[i];
		newArr[this->_size++] = val;

		delete[] this->_elements;
		this->_elements = newArr;
		this->_capacity += this->_resizeFactor;
	}
}

int Vector::pop_back()
{
	if (this->_size == 0) {
		cerr << "error: pop from empty vector" << endl;
		return -9999;
	}
	return this->_elements[--this->_size];
}

void Vector::reserve(int n)
{
	if (n > this->_capacity) {
		//this line computes the new capacity which should be a multiplaction of resizeFactor
		int newLen = (n % this->_resizeFactor == 0) ? n : this->_resizeFactor * (n / this->_resizeFactor) + this->_resizeFactor;
		int* newArr = new int[newLen];
		for (int i = 0; i < this->_size; i++)
			newArr[i] = this->_elements[i];

		delete[] this->_elements;
		this->_elements = newArr;
		this->_capacity = newLen;
	}
}

void Vector::resize(int n)
{
	if (n <= this->_capacity) {
		this->_size = n;
	}
	else
	{
		reserve(n);
		// don't know if this loop is neccessary (?)
		for (int i = this->_size; i < n; i++)
			this->_elements[i] = 0;
		this->_size = n;
	}
}

void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
		this->_elements[i] = val;
}

void Vector::resize(int n, const int& val)
{
	int prevSize = this->_size;
	this->resize(n);
	if (this->_size > prevSize) {//if the arr has been expanded, initialize avabile cells
		for (int i = prevSize; i < this->_size; i++)
			this->_elements[i] = val;
	}
}

Vector::Vector(const Vector& other)
{
	*this = other;
}

Vector& Vector::operator=(const Vector& other)
{
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;

	this->_elements = new int[this->_capacity];
	for (int i = 0; i < this->_size; i++)
		this->_elements[i] = other._elements[i];

	return *this;
}

int& Vector::operator[](int n) const
{
	if (n >= this->_size) {
		cerr << "vector overflow! n is bigger than the vector's size." << endl;
		return this->_elements[0];
	}
	return this->_elements[n];
}
